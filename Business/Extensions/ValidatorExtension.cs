﻿using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business
{
    public static class ValidatorExtension
    {
		public static IServiceCollection AddValidators(this IServiceCollection services)
		{
			services.AddTransient<IValidator<TagInsertOrUpdateDTO>, TagInsertOrUpdateDTOValidator>();
			services.AddTransient<IValidator<TaskInsertOrUpdateDTO>, TaskInsertOrUpdateDTOValidator>();
			services.AddTransient<IValidator<TaskListInsertOrUpdateDTO>, TaskListInsertOrUpdateDTOValidator>();
			
			return services;
		}
	}
}
