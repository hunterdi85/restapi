﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business
{
    public class TagInsertOrUpdateDTOValidator: AbstractValidator<TagInsertOrUpdateDTO>, IValidatorBase
    {
        public TagInsertOrUpdateDTOValidator()
        {
            RuleFor(v => v.Name).NotEmpty().WithMessage("Name is required.");
        }
    }
}
