﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business
{
    public abstract class BaseDomain: BaseDomain<Guid>
    {
    }

    public abstract class BaseDomain<TPK> where TPK: IComparable
    {
        public virtual TPK Id { get; set; }
        public virtual bool Active { get; set; }
        public virtual bool Visible { get; set; }
    }
}
