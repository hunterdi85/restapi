﻿using Architecture;
using Business;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories
{
    public interface ITaskListRepository: IRepositoryBase<TaskList>
    {
    }
}
