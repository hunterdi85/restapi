﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Architecture
{
    public static class DbContextConfigurationExtension
    {
        public static IServiceCollection AddDbContextConfiguration(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContextPool<ApplicationDataContext>(option =>
            {
                option.UseInMemoryDatabase(configuration.GetSection("ConnectionString").Value);
            });

            services.AddScoped<DbContext, ApplicationDataContext>();

            return services;
        }
    }
}
