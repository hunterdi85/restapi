﻿using Business;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Architecture
{
    public class ApplicationDataContext: DbContext
    {
        public DbSet<Tag> Tags { get; set; }
        public DbSet<TaskList> TaskLists { get; set; }
        public DbSet<Business.Tasks> Tasks { get; set; }
        public DbSet<TagTask> TagsTasks { get; set; }

        public ApplicationDataContext(DbContextOptions<ApplicationDataContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }

        public override int SaveChanges()
        {
            ManagerDomainTracker();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            ManagerDomainTracker();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void ManagerDomainTracker()
        {
            ChangeTracker.DetectChanges();

            var changesTracked = ChangeTracker.Entries().Where(x => x.State == EntityState.Deleted 
                || x.State == EntityState.Added || x.State == EntityState.Modified);

            foreach (var item in changesTracked)
            {
                UpdateSoftDeleteStatuses(item);
            }
        }

        private void UpdateSoftDeleteStatuses(EntityEntry item)
        {
            if (item.Entity is BaseDomain entity)
            {
                switch (item.State)
                {
                    case EntityState.Modified:
                        entity.Visible = true;
                        break;
                    case EntityState.Added:
                        entity.Visible = true;
                        entity.Active = true;
                        break;
                    case EntityState.Deleted:
                        item.State = EntityState.Modified;
                        entity.Visible = false;
                        entity.Active = false;
                        break;
                }
            }
        }
    }
}
