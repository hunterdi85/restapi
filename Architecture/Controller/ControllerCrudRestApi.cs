﻿using AutoMapper;
using Business;
using FluentValidation;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Architecture
{
    [EnableCors("RestApi")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ControllerCrudRestApi<TDomain, TDto, TDtoInsertOrUpdate> : ControllerBase 
        where TDomain: BaseDomain where TDto: class where TDtoInsertOrUpdate: class
    {
        protected readonly IServiceBase<TDomain> _serviceCrud;
        protected readonly IMapper _mapper;
        protected readonly IValidator<TDtoInsertOrUpdate> _validator;

        public ControllerCrudRestApi(IServiceBase<TDomain> serviceCrud, IMapper mapper, IValidator<TDtoInsertOrUpdate> validator)
        {
            this._serviceCrud = serviceCrud;
            this._mapper = mapper;
            this._validator = validator;
        }

        [HttpPost]
        public async virtual Task<IActionResult> Create([FromBody] TDtoInsertOrUpdate dto)
        {
            var domain = this._mapper.Map<TDomain>(dto);
            await this._serviceCrud.CreateAsync(domain);

            return Ok();
        }

        [HttpGet]
        public async virtual Task<IActionResult> GetAll()
        {
            var domains = await this._serviceCrud.GetAllAsync();
            var result = this._mapper.Map<ICollection<TDto>>(domains);

            return Ok(result);
        }

        [HttpGet("{id}")]
        public async virtual Task<IActionResult> GetById([FromRoute] Guid id)
        {
            var domain = await this._serviceCrud.GetByIdAsync(id);
            if(domain == null)
            {
                return NotFound();
            }

            var dto = this._mapper.Map<TDto>(domain);

            return Ok(dto);
        }

        [HttpDelete("{id}")]
        public async virtual Task<IActionResult> Delete([FromRoute] Guid id)
        {
            var domain = await this._serviceCrud.GetByIdAsync(id);
            if (domain == null)
            {
                return NotFound();
            }

            await this._serviceCrud.RemoveAsync(id);

            return Ok();
        }

        [HttpPut("{id}")]
        public async virtual Task<IActionResult> Update([FromRoute] Guid id, [FromBody] TDtoInsertOrUpdate dto)
        {
            var domain = await this._serviceCrud.GetByIdAsync(id);
            if (domain == null)
            {
                return NotFound();
            }

            domain = this._mapper.Map<TDomain>(dto);

            await this._serviceCrud.UpdateAsync(id, domain);

            return Ok();
        }
    }
}
