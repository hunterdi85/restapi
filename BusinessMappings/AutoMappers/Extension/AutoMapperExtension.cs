﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessMappings
{
    public static class AutoMapperExtension
    {
		public static IServiceCollection AddMapperConfiguration(this IServiceCollection services)
		{
			var configMapper = new MapperConfiguration(configuration =>
			{
				configuration.AddProfile<TagMapper>();
				configuration.AddProfile<TaskMapper>();
				configuration.AddProfile<TaskListMapper>();
				configuration.AddProfile<TagTaskMapper>();
			});

			services.AddSingleton(configMapper.CreateMapper());

			return services;
		}
	}
}
