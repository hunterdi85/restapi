﻿using Architecture;
using Business;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessMappings
{
    public class TaskListEntityConfiguration : IEntityTypeConfiguration<TaskList>, IEntityMapping
    {
        public void Configure(EntityTypeBuilder<TaskList> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Name).IsRequired();
            
            builder.HasMany(e => e.Tasks)
                .WithOne(e => e.TaskLists)
                .HasForeignKey(e => e.TaskListId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasQueryFilter(e => e.Visible == true);
        }
    }
}
