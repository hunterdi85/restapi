﻿using Architecture;
using Business;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessMappings
{
    public class TagTaskEntityConfiguration : IEntityTypeConfiguration<TagTask>, IEntityMapping
    {
        public void Configure(EntityTypeBuilder<TagTask> builder)
        {
            builder.HasKey(e => e.Id);
            builder.HasIndex(e => new { e.TagId, e.TaskId }).IsUnique();

            builder.HasOne(e => e.Tag)
                .WithMany(e => e.TagsTasks)
                .HasForeignKey(e => e.TaskId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(e => e.Task)
                .WithMany(e => e.TagsTasks)
                .HasForeignKey(e => e.TagId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasQueryFilter(e => e.Visible == true);
        }
    }
}
