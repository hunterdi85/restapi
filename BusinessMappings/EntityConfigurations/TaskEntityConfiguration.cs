﻿using Architecture;
using Business;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessMappings
{
    public class TaskEntityConfiguration : IEntityTypeConfiguration<Tasks>, IEntityMapping
    {
        public void Configure(EntityTypeBuilder<Tasks> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Title).IsRequired();
            builder.Property(e => e.Priority).IsRequired();
            builder.Property(e => e.TaskListId).IsRequired();

            builder.HasQueryFilter(e => e.Visible == true);
        }
    }
}
