﻿using Architecture;
using Business;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BusinessMappings
{
    public class TagEntityConfiguration : IEntityTypeConfiguration<Tag>, IEntityMapping
    {
        public void Configure(EntityTypeBuilder<Tag> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Name).IsRequired();

            builder.HasQueryFilter(e => e.Visible == true);
        }
    }
}
