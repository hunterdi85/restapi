using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NpgsqlTypes;
using Serilog;
using Serilog.Sinks.PostgreSQL;

namespace RestApi
{
    public class Program
    {
        public static int Main(string[] args)
        {
            try
            {
                var hostBuilder = CreateHostBuilder(args).Build();
                Log.Information("Rest Api Starting...");
                hostBuilder.Run();
                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host termineted unexpectedly!");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureLogging((hostingContext, logging) =>
                {
                    logging.ClearProviders();
                    logging.AddSerilog(dispose: true);
                })
                .ConfigureAppConfiguration((hostCtx, config) =>
                {
                    IDictionary<string, ColumnWriterBase> _columnWriters = new Dictionary<string, ColumnWriterBase>
                    {
                        {"message", new RenderedMessageColumnWriter(NpgsqlDbType.Text) },
                        {"message_template", new MessageTemplateColumnWriter(NpgsqlDbType.Text) },
                        {"level", new LevelColumnWriter(true, NpgsqlDbType.Varchar) },
                        {"raise_date", new TimestampColumnWriter(NpgsqlDbType.Timestamp) },
                        {"exception", new ExceptionColumnWriter(NpgsqlDbType.Text) },
                        {"properties", new LogEventSerializedColumnWriter(NpgsqlDbType.Jsonb) },
                        {"props_test", new PropertiesColumnWriter(NpgsqlDbType.Jsonb) },
                        {"machine_name", new SinglePropertyColumnWriter("MachineName", PropertyWriteMethod.ToString, NpgsqlDbType.Text, "l") }
                    };

                    var settings = config.Build();
                    Log.Logger = new LoggerConfiguration()
                        .Enrich.FromLogContext()
                        .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj} {Properties:j}{NewLine}{Exception}")
                        .WriteTo.PostgreSQL(settings.GetSection("ConnectionString").Value, "Logs", _columnWriters)
                        .CreateLogger();
                })
                .UseSerilog()
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                        .UseContentRoot(Directory.GetCurrentDirectory())
                        .UseKestrel()
                        .UseIISIntegration()
                        .UseStartup<Startup>();
                });
    }
}
