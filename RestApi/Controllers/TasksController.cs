﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Architecture;
using AutoMapper;
using Business;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Services;

namespace RestApi.Controllers
{
    public class TasksController : ControllerCrudRestApi<Tasks, TaskDTO, TaskInsertOrUpdateDTO>
    {
        private readonly ITagService _tagService;

        public TasksController(ITaskService serviceCrud, IMapper mapper, IValidator<TaskInsertOrUpdateDTO> validator, ITagService tagService) : base(serviceCrud, mapper, validator)
        {
            this._tagService = tagService;
        }

        public async override Task<IActionResult> GetById([FromRoute] Guid id)
        {
            var task = (await this._serviceCrud.GetByIncludingAsync((e => e.Id == id), true, (e => e.TagsTasks), (e => e.TaskLists))).FirstOrDefault();

            if (task == null)
            {
                NotFound();
            }

            var dto = this._mapper.Map<TaskDTO>(task);

            return Ok(dto);
        }

        public override async Task<IActionResult> Update([FromRoute] Guid id, [FromBody] TaskInsertOrUpdateDTO dto)
        {
            var task = await this._serviceCrud.GetByIdAsync(id);

            if (task == null)
            {
                return NotFound("Not found Task");
            }

            var tagsIds = dto.Tags.Where(e => e.Id != Guid.Empty).Select(e => e.Id).AsEnumerable();
            var tags = await this._tagService.GetByIdsAsync(tagsIds);

            if (tagsIds.Count() != tags.Count)
            {
                return NotFound("Not found Tags.");
            }

            task = this._mapper.Map<Tasks>(dto);

            await this._serviceCrud.UpdateAsync(id, task);

            return Ok();
        }
    }
}
