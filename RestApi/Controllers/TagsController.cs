﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Architecture;
using AutoMapper;
using Business;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Services;

namespace RestApi.Controllers
{
    public class TagsController : ControllerCrudRestApi<Tag, TagDTO, TagInsertOrUpdateDTO>
    {
        public TagsController(ITagService serviceCrud, IMapper mapper, IValidator<TagInsertOrUpdateDTO> validator) : base(serviceCrud, mapper, validator)
        {
        }

        public override async Task<IActionResult> GetById([FromRoute] Guid id)
        {
            var tag = (await this._serviceCrud.GetByIncludingAsync((e => e.Id == id), true, (e => e.TagsTasks))).FirstOrDefault();

            if(tag == null)
            {
                NotFound();
            }

            var dto = this._mapper.Map<TagDTO>(tag);
            return Ok(dto);
        }
    }
}
