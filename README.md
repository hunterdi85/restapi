**Application information**

The application was developed in dot net core 3.1.

---

## Initializing the project

The docker must be set to S.O. linux.

1. At the root of the project, it contains the Makefile, access the project root through the command terminal of your choice.
2. Run the Make build && Make up command, or, if you want to use the docker-compose build command and then the docker-compose up -d command.
3. The application is running on port 443.
4. And when accessing http: // localhost: 443 / swagger, the documentation for routes, endpoints and DTOs will be displayed.

---